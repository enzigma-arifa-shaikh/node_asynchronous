
import { promises } from 'fs';
import { User } from '../model/user'
var fs = require("fs");
const readlineSync = require('readline-sync');
let objUser: any = new User('a', 'b', 'c', 'd', 'e');
function getUserData(user: any) {
    for (let key in objUser) {
        if (typeof (objUser[key]) == 'string') {
            user[key] = readlineSync.question('Enter your ' + key + ' : ');
        }
    }
   
    return user;
}

 export function newUser(user: any): Promise<User> {
    return new Promise(function (onResolve , onReject) {
                let user1 :any= getUserData(user);
                fs.writeFile("data/userDetails.json", JSON.stringify(user1),function(err:any) {
                    if(err) {
                        return console.log(err);
                    }
                });
               
                onResolve(user1);

                onReject('error');
                })
 }

